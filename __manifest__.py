# -*- coding: utf-8 -*-
{
    'name': "Remove amount next to Itbis on Invoice",

    'summary': """
        Remove amount next to Itbis on Invoice""",

    'description': """
        Remove amount next to Itbis on Invoice
    """,

    'author': "Kevin Jimenez",
    'website': "https://katana.do",

    'category': 'Uncategorized',
    'version': '1.0',

    'depends': ['base'],

    'data': [
        'views/templates.xml',
    ],
}
